import utility as u
import requests
import json
from datetime import datetime as dt
from pykakasi import kakasi
import pandas as pd
import re

GLOBAL_DOCOMO_APIKEY = "6a332f56316e32774f416c7973657075746d584f695a707542354f6f52504c725751726648663431442e30"
#GLOBAL_DOCOMO_APIKEY  = "2e413471337a393246652e3550676c5862395878695563662e4e345531625564734d5867763278416d5330"

def docomoHiragana(output_type, sentence):
    endpoint = "https://api.apigw.smt.docomo.ne.jp/gooLanguageAnalysis/v1/hiragana?APIKEY=" + GLOBAL_DOCOMO_APIKEY
    request_id = str(int(dt.now().timestamp() * 1000000))
    item_data = { "request_id":request_id, "sentence":sentence, "output_type":output_type }
    headers={'Content-Type': "application/json"}

    response = requests.post(endpoint, headers=headers, json=item_data)
    response_json = json.loads(response.text)
    print(response_json)
    return response_json["converted"]
"""
text = "渡月橋にヴィレッジがある"

result = u.pykakasiHiragana(text)

print(result)
print(u.pykakasiHiragana2romaji( resut ))

#print(docomoHiragana("hiragana", text))
"""

def edit_sale(df_jp):
    #df_jp = pd.read_csv("../Target/sale/sale.csv", encoding='cp932', dtype=str)
    df_jp = pd.read_csv("../Target/sale/sale.csv", encoding='utf_8', dtype=str)
    lst = list(range(1, len(df_jp.columns)))
    df_jp = df_jp.iloc[:, lst]
    df_jp.to_csv("sale_editted.txt", index = None)

#df = pd.read_csv("./sale_osaka.csv", encoding='utf_8', dtype=str)
#print(df)

def cnv_wakreki2ad(text):

    pattern = r"\A.*\d+年"
    result = re.match(pattern, text)
    if result:
        reki = result.group()[0:2]
        year = result.group()[2:-1]
        if reki == "平成":
            year = str(1988 + int(year))
        elif reki == "昭和":
            year = str(1926 + int(year))
        elif reki == "令和":
            year = str(2019 + int(year))
    else:
        raise ValueError("error!")

    pattern = r"\d+月"
    result = re.search(pattern, text)
    if result:
        month = result.group()[:-1]
    else:
        raise ValueError("error!")

    pattern = r"\d+日"
    result = re.search(pattern, text)
    if result:
        day = result.group()[:-1]
    else:
        raise ValueError("error!")

    return year + "-" + month + "-" + day
    
    #pattern  = r"\d+月"  



print(cnv_wakreki2ad("平成19年11月15日"))

print(u.cnv_property_type("売マンション"))